﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ARP_Base_DLL.Handler;

namespace ARP_Base_DLL
{
    public static class Base
    {
        private static B_MessageHandler message;

        public static B_MessageHandler Message { get { return message; } }

        private static B_WarningHandler warning;

        public static B_WarningHandler Warning { get { return warning; } }

        private static B_ErrorHandler error;

        public static B_ErrorHandler Error { get { return error; } }



        public static void Initialize(B_Handler.Behaviour MessageBehaviour, B_Handler.Behaviour WarningBehaviour, B_Handler.Behaviour ErrorBehaviour)
        {
            message = new B_MessageHandler(MessageBehaviour);

            warning = new B_WarningHandler(WarningBehaviour);

            error = new B_ErrorHandler(ErrorBehaviour);
        }
    }
}
