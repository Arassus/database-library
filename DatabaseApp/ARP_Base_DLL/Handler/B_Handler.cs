﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARP_Base_DLL
{
    public abstract class B_Handler
    {
        public delegate void Behaviour(params object[] Parameters);

        private Behaviour behaviour;

        public void Handle(params object[] Parameters)
        {
            if(this.behaviour != null)
            {
                this.behaviour(Parameters);
            }
        }

        public B_Handler(Behaviour behaviour)
        {
            this.behaviour = new Behaviour(behaviour);
        }

        public enum HandlerModeEnum
        {
            Error,
            Message,
            Warning
        }

        public enum PriorityEnum
        {
            Low,
            Medium,
            High
        }
    }
}
