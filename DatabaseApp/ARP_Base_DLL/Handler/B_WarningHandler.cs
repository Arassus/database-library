﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARP_Base_DLL.Handler
{
    public class B_WarningHandler : B_Handler
    {
        private HandlerModeEnum handlerMode = HandlerModeEnum.Error;

        /// <summary>
        /// Contains the mode of a particular Handler
        /// 
        /// This particular Handler processes Warnings
        /// </summary>
        public HandlerModeEnum HandlerMode { get { return handlerMode; } }



        public B_WarningHandler(Behaviour behaviour) : base(behaviour)
        {
        }
    }
}
