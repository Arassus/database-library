﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARP_Base_DLL.Request
{
    /// <summary>
    /// Represents a Response for the performed Request in the ARP_Base_DLL
    /// </summary>
    public class B_Request_Response
    {
        public enum ResponseEnum
        {
            Success,

            Failure,
            
            Assigned,

            Created
        }

        string message;

        /// <summary>
        /// Contains the Message of the Response
        /// </summary>
        public string Message
        {
            get
            {
                return message;
            }
        }



        B_Request_Instance request;

        /// <summary>
        /// Contains the parent Request of the Response
        /// </summary>
        public B_Request_Instance Request
        {
            get
            {
                return request;
            }
        }



        ResponseEnum response;

        /// <summary>
        /// Contains the Response Enum of the Response
        /// </summary>
        public ResponseEnum Response
        {
            get
            {
                return response;
            }

            set
            {
                response = value;
            }
        }



        public B_Request_Response(B_Request_Instance Request)
        {
            response = ResponseEnum.Created;

            this.request = Request; 
        }



        public override string ToString()
        {
            return null;
        }
    }
}
