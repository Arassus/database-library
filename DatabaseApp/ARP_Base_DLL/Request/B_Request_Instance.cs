﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARP_Base_DLL.Request
{
    /// <summary>
    /// Represents an Instance of a performed Request in the ARP_Base_DLL 
    /// </summary>
    public class B_Request_Instance
    {
        string header;

        /// <summary>
        /// Contains the Header of the Request
        /// </summary>
        public string Header
        {
            get
            {
                return header;
            }
        }



        List<B_Request_Parameter> parameters = new List<B_Request_Parameter>();

        /// <summary>
        /// Contains the Parameter List of the Request
        /// </summary>
        public List<B_Request_Parameter> Parameters
        {
            get
            {
                return parameters;
            }
        }



        B_Request_Response response;

        public B_Request_Response Response
        {
            get
            {
                return response;
            }

            set
            {
                response = value;
            }
        }

        public B_Request_Instance(string header)
        {
            response = new B_Request_Response(this);

            this.header = header;
        }

        public void AddParameter(string ParameterName, object ParameterValue)
        {
            parameters.Add(new B_Request_Parameter(ParameterName, ParameterValue, this));
        }

        public void RemoveParamter(string ParameterName)
        {
            B_Request_Parameter RemovedParameter = null;

            foreach(B_Request_Parameter parameter in parameters)
            {
                if (parameter.Name == ParameterName)
                {
                    RemovedParameter = parameter;
                }
            }

            if(RemovedParameter != null)
            {
                parameters.Remove(RemovedParameter);
            }
        }
    }
}
