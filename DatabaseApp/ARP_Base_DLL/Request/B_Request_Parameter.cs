﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARP_Base_DLL.Request
{
    /// <summary>
    /// Represents a Parameter of a performed Request in the ARP_Base_DLL
    /// </summary>
    public class B_Request_Parameter
    {
        string name;

        /// <summary>
        /// Contains the Name of the Paramater
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }

        object value;

        /// <summary>
        /// Contains the Value of the Parameter
        /// </summary>
        public object Value
        {
            get
            {
                return value;
            }
        }

        B_Request_Instance request;

        /// <summary>
        /// Contains a Request to which the Parameter is assigned to
        /// </summary>
        public B_Request_Instance Request
        {
            get
            {
                return request;
            }
        }

        public B_Request_Parameter(string name, object value, B_Request_Instance request)
        {
            Base.Message.Handle("BASE : Creating new REUQEST PARAMETER", B_Handler.PriorityEnum.Medium);

            try
            {
                bool NameCanBeAssigned = CanBeAssigned_Name(name);

                if (NameCanBeAssigned)
                {
                    Base.Message.Handle("Assigning NAME . . .", B_Handler.PriorityEnum.Low);

                    this.name = name;

                    Base.Message.Handle("NAME has been assigned successfully", B_Handler.PriorityEnum.Low);
                }

                bool ValueCanBeAssigned = CanBeAssigned_Value(value);

                if (ValueCanBeAssigned)
                {
                    Base.Message.Handle("Assigning VALUE . . .", B_Handler.PriorityEnum.Low);

                    this.value = value;

                    Base.Message.Handle("VALUE has been assigned successfully", B_Handler.PriorityEnum.Low);
                }

                bool RequestCanBeAssigned = CanBeAssigned_Request(request);

                if(RequestCanBeAssigned)
                {
                    Base.Message.Handle("Assigning REQUEST . . .", B_Handler.PriorityEnum.Low);

                    this.request = request;

                    Base.Message.Handle("REQUEST has been assigned successfully", B_Handler.PriorityEnum.Low);
                }

                request.Response.Response = B_Request_Response.ResponseEnum.Assigned;
            }
            catch(Exception exception)
            {
                request.Response.Response = B_Request_Response.ResponseEnum.Failure;

                Base.Error.Handle(exception);
            }
        }

        bool CanBeAssigned_Name(string name)
        {
            Base.Message.Handle("Can NAME be assigned . . .", B_Handler.PriorityEnum.Low);

            bool CanBeAssigned = !(string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name));
            
            Base.Message.Handle(CanBeAssigned.ToString(), B_Handler.PriorityEnum.Low);

            return CanBeAssigned;
        }

        bool CanBeAssigned_Value(object value)
        {
            Base.Message.Handle("Can VALUE be assigned . . .");

            bool CanBeAssigned = (value != null);

            Base.Message.Handle(CanBeAssigned.ToString(), B_Handler.PriorityEnum.Low);

            return CanBeAssigned;
        }

        bool CanBeAssigned_Request(B_Request_Instance request)
        {
            Base.Message.Handle("Can REQUEST be assigned . . .", B_Handler.PriorityEnum.Low);

            bool CanBeAssigned = (request != null);

            Base.Message.Handle(CanBeAssigned.ToString(), B_Handler.PriorityEnum.Low);

            return CanBeAssigned;
        }
    }
}
