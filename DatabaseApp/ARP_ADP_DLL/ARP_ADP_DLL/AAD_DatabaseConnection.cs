﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

using System.Data;
//using System.Data.SqlClient;

using ARP_Base_DLL;
using ARP_Base_DLL.Handler;
using ARP_Base_DLL.Request;

//using MySql.Data;
//using MySql.Data.MySqlClient;

namespace ARP_ADP_DLL
{
    public abstract class AAD_DatabaseConnection
    {
        //    public Response CheckConnection(string ConnectionString)
        //    {
        //        bool result = false;

        //        MySqlConnection connection = new MySqlConnection(ConnectionString);

        //        try

        //        {
        //            connection.Open();

        //            result = true;

        //            connection.Close();

        //            return Response.Success;
        //        }
        //        catch(Exception exception)
        //        {
        //            Base.Error.Handle(exception);

        //            result = false;

        //            return Response.Failure;
        //        }
        //    }

        //    public Response Connect(string Server, string IntegratedSecurity, string UserInstance)
        //    {
        //        string server = (string.IsNullOrWhiteSpace(Server)) ? "" : ("Data Source=" + Server + "; ");
        //        string integratedSecurity = (string.IsNullOrWhiteSpace(IntegratedSecurity)) ? "" : ("Integrated Security=" + IntegratedSecurity + "; ");
        //        string userInstance = (string.IsNullOrWhiteSpace(UserInstance)) ? "" : ("User Instance=" + UserInstance + "; ");

        //        string ConnectionString = String.Format(server + integratedSecurity + userInstance);

        //        Response response = Response.Failure;

        //        try
        //        {
        //            SqlConnection connection = new SqlConnection(ConnectionString);

        //            connection.Open();

        //            connection.Close();

        //            response = Response.Success;
        //        }
        //        catch(Exception exception)
        //        {
        //            Base.Error.Handle(exception);
        //        }

        //        return response;
        //    }

        //    public Response CreateDatabase(string Server, string Database, string IntegratedSecurity, string UserInstance)
        //    {
        //        Response response = Response.Failure;

        //        try
        //        {
        //            using (var connection = new SqlConnection(String.Format(
        //                "Data Source=" + Server + "; " +
        //                "Initial Catalog=" + Database + "; " +
        //                "Integrated Security=" + IntegratedSecurity + "; " +
        //                "User Instance=" + UserInstance + "; "
        //                )))
        //            {
        //                connection.Open();

        //                using (var command = connection.CreateCommand())
        //                {
        //                    command.CommandText = String.Format("if not exists(select * from sys.databases where name = '" + Database + "') create database " + Database);

        //                    int iresponse = command.ExecuteNonQuery();

        //                    if (iresponse == 0)
        //                    {
        //                        Base.Message.Handle("Database '" + Database + "' already exists on server '" + Server + "'");
        //                    }
        //                    else
        //                    {
        //                        Base.Message.Handle("Database '" + Database + "' has been created successfully on server '" + Server + "'");
        //                    }
        //                }

        //                connection.Close();
        //            }
        //        }
        //        catch (Exception exception)
        //        {
        //            Base.Error.Handle(exception);
        //        }

        //        return response;
        //    }

        //    SqlDataAdapter adapter;

        //    public Response CreateMdfFile(string Database)
        //    {
        //        Response response = Response.Failure;

        //        SaveFileDialog dialog = new SaveFileDialog();

        //        try
        //        {
        //            dialog.Filter = "SQL Server Database File (*.mdf)|*.mdf|All files (*.*)|*.*";

        //            dialog.FilterIndex = 1;

        //            dialog.RestoreDirectory = true;

        //            dialog.InitialDirectory = Environment.CurrentDirectory;

        //            dialog.FileName = Database;

        //            DialogResult result = dialog.ShowDialog();

        //            if (result == DialogResult.OK)
        //            {
        //                dialog.OpenFile();

        //                response = Response.Success;
        //            }
        //        }
        //        catch (Exception exception)
        //        {
        //            response = Response.Failure;

        //            Base.Error.Handle(exception);
        //        }

        //        return response;
        //    }

        //    public Response GetDataSet(string Server, string Database, string IntegratedSecurity, string UserInstance, string CommandString, string SourceTable, out DataSet dataset)
        //    {
        //        string ConnectionString = String.Format(
        //               //@"Data Source=(LocalDB)\MSSQLLocalDB; Integrated Security=true;"
        //               "Data Source=" + Server + ";" +
        //               "Integrated Security=" + IntegratedSecurity + ";" +
        //               "User Instance=" + UserInstance + ";"
        //               );

        //        Response response = Response.Failure;

        //        dataset = new DataSet();

        //        try
        //        {
        //            SqlConnection connection = new SqlConnection();

        //            connection.ConnectionString = ConnectionString;

        //            connection.Open();

        //            adapter = new SqlDataAdapter(CommandString, connection);

        //            adapter.Fill(dataset, SourceTable);

        //            response = Response.Success;

        //            connection.Close();
        //        }
        //        catch (Exception exception)
        //        {
        //            Base.Error.Handle(exception);
        //        }

        //        return response;
        //    }

        //    public Response PerformQuery(string Server, string IntegratedSecurity, string UserInstance, string CommandString, out DataTable dataTable)
        //    {
        //        string ConnectionString = String.Format(
        //               //@"Data Source=(LocalDB)\MSSQLLocalDB; Integrated Security=true;"
        //               "Data Source=" + Server + ";" +
        //               "Integrated Security=" + IntegratedSecurity + ";" +
        //               "User Instance=" + UserInstance + ";"
        //               );

        //        Response response = Response.Failure;

        //        dataTable = new DataTable();

        //        try
        //        {
        //            SqlConnection connection = new SqlConnection(ConnectionString);

        //            connection.Open();

        //            using (var command = connection.CreateCommand())
        //            {
        //                command.CommandText = String.Format(CommandString);

        //                dataTable.Load(command.ExecuteReader());
        //            }

        //            response = Response.Success;
        //        }
        //        catch (Exception exception)
        //        {
        //            Base.Error.Handle(exception);
        //        }

        //        return response;
        //    }

        //    public Response Update(DataSet dataSet)
        //    {
        //        Response response = Response.Failure;

        //        try
        //        {
        //            SqlCommandBuilder command = new SqlCommandBuilder(adapter);

        //            adapter.Update(dataSet, "Table");
        //        }
        //        catch(Exception exception)
        //        {
        //            Base.Error.Handle(exception);
        //        }

        //        return response;
        //    }

        //    private string GetConnectionString()
        //    {
        //        string ConnectionString = "";

        //        return ConnectionString;
        //    }

        public enum Response
        {
            Success,

            Failure
        }

        private B_Request_Instance request;

        /// <summary>
        /// 
        /// </summary>
        public B_Request_Instance Request
        {
            get
            {
                return request;
            }

            set
            {
                request = value;
            }
        }
    }
}