﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ARP_Base_DLL;
using ARP_Base_DLL.Request;
using MySql.Data.MySqlClient;

namespace ARP_ADP_DLL.SQL_MY
{
    public class AAD_DC_MySql : AAD_DatabaseConnection, AAD_IDatabaseConnection
    {
        public AAD_DC_MySql()
        {
            Request = new B_Request_Instance("MySql Connection");

            Request.Response = new B_Request_Response(Request);
        }

        bool MustHave_Server = false;
        bool MustHave_Database = false;
        bool MustHave_User = false;
        bool MustHave_Password = false;
        bool MustHave_Port = false;

        bool MustHave_DataSet = false;
        bool MustHave_Query = false;


        string currentQuery = "";

        public string CurrentQuery { get { return currentQuery; } }


        private void Setup_MustHave(bool MustHave_DataSet)
        {
            Setup_MustHave(true, true, true, true, true);

            this.MustHave_DataSet = MustHave_DataSet;
        }

        private void Setup_MustHave(bool MustHave_Server, bool MustHave_Port, bool MustHave_Database, bool MustHave_User, bool MustHave_Password)
        {
            this.MustHave_Server = MustHave_Server;

            this.MustHave_Port = MustHave_Port;

            this.MustHave_Database = MustHave_Database;

            this.MustHave_User = MustHave_User;

            this.MustHave_Password = MustHave_Password;
        }
        
        private void Setup_MustHave(bool MustHave_Server, bool MustHave_Port, bool MustHave_Database, bool MustHave_User, bool MustHave_Password, bool MustHave_DataSet, bool MustHave_Query)
        {
            this.MustHave_Server = MustHave_Server;

            this.MustHave_Port = MustHave_Port;

            this.MustHave_Database = MustHave_Database;

            this.MustHave_User = MustHave_User;

            this.MustHave_Password = MustHave_Password;

            this.MustHave_DataSet = MustHave_DataSet;

            this.MustHave_Query = MustHave_Query;
        }

        MySqlConnectionStringBuilder ConnectionStringBuilder;

        MySqlConnection Connection;

        public B_Request_Response.ResponseEnum CloseConnection()
        {
            Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

            try
            {
                Connection.Close();

                Request.Response.Response = B_Request_Response.ResponseEnum.Success;
            }
            catch (Exception exception)
            {
                Base.Error.Handle(exception);

                Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
            }

            return Request.Response.Response;
        }

        public B_Request_Response.ResponseEnum Create_Database()
        {
            Setup_MustHave(true, true, false, true, true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    Connection.Open();

                    B_Request_Parameter DatabaseParameter = null;

                    foreach(B_Request_Parameter parameter in Request.Parameters)
                    {
                        Base.Message.Handle(parameter.Name + " : " + parameter.ToString(), B_Handler.PriorityEnum.High);

                        if(parameter.Name.ToLower().Contains("database"))
                        {
                            DatabaseParameter = parameter;

                            break;
                        }
                    }

                    if(DatabaseParameter == null)
                    {
                        throw new Exception("Database parameter has not been set");
                    }

                    string CreateDatabaseQuery = "CREATE DATABASE IF NOT EXISTS `" + DatabaseParameter.Value.ToString() + "`;";

                    MySqlCommand Command = new MySqlCommand(CreateDatabaseQuery, Connection);

                    int Result = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }
            }

            return Request.Response.Response;
        }

        public B_Request_Response.ResponseEnum Create_Table()
        {
            Setup_MustHave(true, true, true, true, true, false, false);

            Request.Response.Response = AssignParameters();
            
            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    Connection.Open();
                    
                    DataTable Table = Local_Data.Tables[0];

                    currentQuery = "CREATE TABLE IF NOT EXISTS " + Table.TableName + "( ";

                    bool FirstColumn = true;

                    foreach(DataColumn Column in Table.Columns)
                    {
                        if(FirstColumn)
                        {
                            FirstColumn = false;
                        }
                        else
                        {
                            currentQuery += ", ";
                        }

                        currentQuery += Column.ColumnName + " ";

                        currentQuery += GetMySqlType(Column.DataType.ToString(), Column.AllowDBNull);

                        currentQuery += (Column.AutoIncrement) ? " AUTO_INCREMENT " : "";
                    }

                    DataColumn[] PrimaryKeys = Table.PrimaryKey;

                    if(PrimaryKeys.Length > 0)
                    {
                        currentQuery += ", PRIMARY KEY ( " + PrimaryKeys[0].ColumnName + " ) ";
                    }

                    currentQuery += ") ENGINE=INNODB;";

                    MySqlCommand Command = new MySqlCommand(currentQuery, Connection);

                    Base.Message.Handle(currentQuery, B_Handler.PriorityEnum.High);

                    int Response = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }

                return Request.Response.Response;
            }


            return B_Request_Response.ResponseEnum.Failure;
        }

        private string GetMySqlType(string DotNetType, bool CanBeNullable)
        {
            string MySqlType = "";

            switch(DotNetType)
            {
                case "System.Boolean":
                    MySqlType += "boolean";
                    break;

                case "System.Byte":
                    MySqlType += "tinyint";
                    break;

                case "System.Byte[]":
                    MySqlType += "blob";
                    break;

                case "System.Data.Spatial.DbGeometry":
                    MySqlType += "geometry";
                    break;

                case "System.Data.Spatial.DbGeography":
                    MySqlType += "geometry";
                    break;

                case "System.DateTime":
                    MySqlType += "datetime";
                    break;

                case "System.DateTimeOffset":
                    MySqlType += "datetime";
                    break;

                case "System.Decimal":
                    MySqlType += "decimal";
                    break;

                case "System.Double":
                    MySqlType += "double";
                    break;

                case "System.Guid":
                    MySqlType += "char(36)";
                    break;

                case "System.Int16":
                    MySqlType += "smallint";
                    break;

                case "System.Int32":
                    MySqlType += "int";
                    break;

                case "System.Int64":
                    MySqlType += "bigint";
                    break;

                case "System.SByte":
                    MySqlType += "tinyint";
                    break;

                case "System.Single":
                    MySqlType += "float";
                    break;

                case "System.String":
                    MySqlType += "text";
                    break;
                    
                case "System.TimeSpan":
                    MySqlType += "time";
                    break;

                default:
                    MySqlType += "text";
                    break;
            }

            MySqlType += (CanBeNullable) ? " NULL" : " NOT NULL";

            return MySqlType;
        }

        public B_Request_Response.ResponseEnum OpenConnection()
        {
            Setup_MustHave(true, true, false, true, true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    Connection.Open();

                    currentQuery = "OPEN CONNECTION;";

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;
                }
                catch(Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }
            }

            return Request.Response.Response;
        }

        public B_Request_Response.ResponseEnum Insert_Value()
        {
            Setup_MustHave(true, true, true, true, true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    Connection.Open();

                    DataTable Table = Local_Data.Tables[0];

                    currentQuery = "INSERT INTO " + Table.TableName + "( ";

                    bool FirstColumn = true;

                    foreach (DataColumn Column in Table.Columns)
                    {
                        if (FirstColumn)
                        {
                            FirstColumn = false;
                        }
                        else
                        {
                            currentQuery += ", ";
                        }

                        currentQuery += Column.ColumnName;
                    }

                    currentQuery += " ) VALUES( \"";

                    FirstColumn = true;

                    foreach (DataRow Row in Table.Rows)
                    {
                        foreach (DataColumn Column in Table.Columns)
                        {
                            if (FirstColumn)
                            {
                                FirstColumn = false;
                            }
                            else
                            {
                                currentQuery += "\" ), ( \"";
                            }

                            currentQuery += Row[Column.ColumnName];
                        }
                    }

                    currentQuery += "\" );";

                    MySqlCommand Command = new MySqlCommand(currentQuery, Connection);

                    Base.Message.Handle(currentQuery, B_Handler.PriorityEnum.High);

                    int Response = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }
            }

            return Request.Response.Response;
        }
   
        public B_Request_Response.ResponseEnum Update()
        {
            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    currentQuery = GetUpdateQuery();

                    Base.Message.Handle(currentQuery, B_Handler.PriorityEnum.High);

                    Connection.Open();

                    MySqlCommand UpdateCommand = new MySqlCommand(currentQuery, Connection);

                    int UpdateResponse = UpdateCommand.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }
            }

            return Request.Response.Response;
        }

        private string GetUpdateQuery()
        {
            string UpdateQuery = "";

            for (int i = 0; i < Database_Data.Tables.Count; i++)
            {
                var Original = Database_Data.Tables[i].AsEnumerable().Except(Local_Data.Tables[i].AsEnumerable(), DataRowComparer.Default);

                var Difference = Local_Data.Tables[i].AsEnumerable().Except(Database_Data.Tables[i].AsEnumerable(), DataRowComparer.Default);
                
                DataColumnCollection Columns = Local_Data.Tables[i].Columns;

                for (int j = 0; j < Difference.Count(); j++)
                {
                    DataRow DifferenceRow = (DataRow)Difference.ToArray()[j];

                    DataRow OriginalRow = (DataRow)Original.ToArray()[j];

                    object[] OriginalItems = OriginalRow.ItemArray;

                    object[] DifferenceItems = DifferenceRow.ItemArray;

                    UpdateQuery += "UPDATE " + Database_Data.Tables[i].TableName + " SET ";
                    bool FirstColumn = true;
                    for (int ColumnId = 0; ColumnId < Columns.Count; ColumnId++)
                    {
                        if (FirstColumn)
                        {
                            FirstColumn = false;
                        }
                        else
                        {
                            UpdateQuery += ", ";
                        }

                        //Type Correction - ''-string; _-int
                        UpdateQuery += Columns[ColumnId].ColumnName + " = '" + DifferenceItems[ColumnId].ToString() + "'";
                    }

                    UpdateQuery += " WHERE ";
                    FirstColumn = true;
                    for (int ColumnId = 0; ColumnId < Columns.Count; ColumnId++)
                    {
                        if (FirstColumn)
                        {
                            FirstColumn = false;
                        }
                        else
                        {
                            UpdateQuery += "AND ";
                        }

                        UpdateQuery += Columns[ColumnId].ColumnName + " = '" + OriginalItems[ColumnId].ToString() + "'";
                    }

                    UpdateQuery += "; ";
                }
            }

            return UpdateQuery;
        }



        public DataSet Database_Data = new DataSet();
        public DataSet Local_Data = new DataSet();

        public B_Request_Response.ResponseEnum AssignParameters()
        {
            try
            {
                ConnectionStringBuilder = new MySqlConnectionStringBuilder();

                foreach (B_Request_Parameter Parameter in Request.Parameters)
                {
                    SetParameter_ConnectionString(MustHave_Server, "server", Parameter);

                    SetParameter_ConnectionString(MustHave_Database, "database", Parameter);

                    SetParameter_ConnectionString(MustHave_Port, "port", Parameter);

                    SetParameter_ConnectionString(MustHave_User, "user", Parameter);

                    SetParameter_ConnectionString(MustHave_Password, "password", Parameter);

                    SetParameter_DataSet(MustHave_DataSet, "dataset", Parameter);

                    SetParameter_Query(MustHave_Query, "query", Parameter);
                }

                return B_Request_Response.ResponseEnum.Assigned;
            }
            catch(Exception exception)
            {
                Base.Error.Handle(exception);
                
                return B_Request_Response.ResponseEnum.Failure;
            }
        }

        void SetParameter_ConnectionString(bool MustHave, string Keyword, B_Request_Parameter Parameter)
        {
            if (MustHave)
            {
                if (Parameter.Name.ToLower() == Keyword.ToLower())
                {
                    ConnectionStringBuilder.Add(Parameter.Name, Parameter.Value);
                }
            }
        }

        void SetParameter_DataSet(bool MustHave, string Keyword, B_Request_Parameter Parameter)
        {
            if (MustHave)
            {
                if (Parameter.Name.ToLower() == Keyword.ToLower())
                {
                    //Local_Data = new DataSet();
                    Local_Data.Tables.Clear();
                    //Database_Data = new DataSet();
                    Database_Data.Tables.Clear();

                    DataSet data = (DataSet) Parameter.Value;
                    
                    foreach (DataTable Table in data.Tables)
                    {
                        DataTable Local_NewTable = new DataTable(Table.TableName);
                        Local_NewTable.TableName = Table.TableName;
                        DataTable Database_NewTable = new DataTable(Table.TableName);
                        Database_NewTable.TableName = Table.TableName;

                        foreach (DataColumn Column in Table.Columns)
                        {
                            DataColumn Local_NewColumn = new DataColumn();
                            DataColumn Database_NewColumn = new DataColumn();

                            SetColumn(ref Local_NewColumn, Column.AllowDBNull, Column.AutoIncrement, Column.ColumnName, Column.DataType, Column.DefaultValue, Column.Unique);
                            SetColumn(ref Database_NewColumn, Column.AllowDBNull, Column.AutoIncrement, Column.ColumnName, Column.DataType, Column.DefaultValue, Column.Unique);

                            Local_NewTable.Columns.Add(Local_NewColumn);
                            Database_NewTable.Columns.Add(Database_NewColumn);
                        }

                        foreach(DataRow Row in Table.Rows)
                        {
                            DataRow Local_NewRow = Local_NewTable.NewRow();
                            DataRow Database_NewRow = Database_NewTable.NewRow();

                            Local_NewRow.ItemArray = Row.ItemArray;
                            Database_NewRow.ItemArray = Row.ItemArray;

                            Local_NewTable.Rows.Add(Local_NewRow);
                            Database_NewTable.Rows.Add(Database_NewRow);
                        }

                        Local_Data.Tables.Add(Local_NewTable);
                        Database_Data.Tables.Add(Database_NewTable);
                    }
                    
                }
            }
        }

        void SetParameter_Query(bool MustHave, string Keyword, B_Request_Parameter Parameter)
        {
            if (MustHave)
            {
                if (Parameter.Name.ToLower() == Keyword.ToLower())
                {
                    currentQuery = Parameter.Value.ToString();
                }
            }
        }

        void SetColumn(ref DataColumn column, bool AllowDBNull, bool AutoIncrement, string ColumnName, Type DataType, object DefaultValue, bool Unique)
        {
            column.AllowDBNull = AllowDBNull;

            column.AutoIncrement = AutoIncrement;

            column.ColumnName = ColumnName;

            column.DataType = DataType;

            column.DefaultValue = DefaultValue;

            column.Unique = Unique;
        }

        public B_Request_Response.ResponseEnum Select_Table()
        {
            Setup_MustHave(true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {                
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    string TableName = Database_Data.Tables[0].TableName;

                    MySqlCommand Command = new MySqlCommand("SELECT * FROM " + TableName + ";", Connection);

                    MySqlDataAdapter Adapter = new MySqlDataAdapter(Command);

                    Database_Data.Tables.Clear();
                    Adapter.Fill(Database_Data);
                    Database_Data.Tables[0].TableName = TableName;

                    Local_Data.Tables.Clear();
                    Adapter.Fill(Local_Data);
                    Local_Data.Tables[0].TableName = TableName;

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }
            }

            return Request.Response.Response;
        }

        public B_Request_Response.ResponseEnum ExecuteQuery(string Query)
        {
            Setup_MustHave(true, true, true, true, true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    Connection.Open();

                    MySqlCommand Command = new MySqlCommand(Query, Connection);

                    Base.Message.Handle(Query, B_Handler.PriorityEnum.High);

                    int Response = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }

                return Request.Response.Response;
            }


            return B_Request_Response.ResponseEnum.Failure;
        }

        public B_Request_Response.ResponseEnum List_Databases()
        {
            Setup_MustHave(true, true, false, true, true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    currentQuery = "SHOW DATABASES;";

                    Connection.Open();

                    MySqlCommand Command = new MySqlCommand(currentQuery, Connection);

                    MySqlDataAdapter Adapter = new MySqlDataAdapter(Command);

                    Database_Data.Tables.Clear();
                    Adapter.Fill(Database_Data);
                    Database_Data.Tables[0].TableName = "Databases";

                    Local_Data.Tables.Clear();
                    Adapter.Fill(Local_Data);
                    Local_Data.Tables[0].TableName = "Databases";

                    //int Response = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }

                return Request.Response.Response;
            }


            return B_Request_Response.ResponseEnum.Failure;
        }

        public B_Request_Response.ResponseEnum ExecuteQuery()
        {
            Setup_MustHave(true, true, false, true, true, false, true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    Connection.Open();

                    MySqlCommand Command = new MySqlCommand(currentQuery, Connection);

                    MySqlDataAdapter Adapter = new MySqlDataAdapter(Command);

                    Database_Data.Tables.Clear();
                    Adapter.Fill(Database_Data);
                    Database_Data.Tables[0].TableName = "Query";

                    Local_Data.Tables.Clear();
                    Adapter.Fill(Local_Data);
                    Local_Data.Tables[0].TableName = "Query";

                    //int Response = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }

                return Request.Response.Response;
            }


            return B_Request_Response.ResponseEnum.Failure;
        }

        public B_Request_Response.ResponseEnum List_Tables()
        {
            Setup_MustHave(true, true, true, true, true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    currentQuery = "SHOW TABLES;";

                    Connection.Open();

                    MySqlCommand Command = new MySqlCommand(currentQuery, Connection);

                    MySqlDataAdapter Adapter = new MySqlDataAdapter(Command);

                    Database_Data.Tables.Clear();
                    Adapter.Fill(Database_Data);
                    Database_Data.Tables[0].TableName = "Tables";

                    Local_Data.Tables.Clear();
                    Adapter.Fill(Local_Data);
                    Local_Data.Tables[0].TableName = "Tables";

                    int Response = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }

                return Request.Response.Response;
            }


            return B_Request_Response.ResponseEnum.Failure;
        }

        public B_Request_Response.ResponseEnum Describe_Table()
        {
            Setup_MustHave(true);

            Request.Response.Response = AssignParameters();

            if (Request.Response.Response != B_Request_Response.ResponseEnum.Failure)
            {
                Connection = new MySqlConnection(ConnectionStringBuilder.ConnectionString);

                try
                {
                    DataTable Table = Local_Data.Tables[0];

                    string TableName = Table.TableName;

                    currentQuery = "DESCRIBE " + TableName + ";";

                    Connection.Open();

                    MySqlCommand Command = new MySqlCommand(currentQuery, Connection);

                    MySqlDataAdapter Adapter = new MySqlDataAdapter(Command);

                    Database_Data.Tables.Clear();
                    Adapter.Fill(Database_Data);
                    Database_Data.Tables[0].TableName = TableName;

                    Local_Data.Tables.Clear();
                    Adapter.Fill(Local_Data);
                    Local_Data.Tables[0].TableName = TableName;

                    int Response = Command.ExecuteNonQuery();

                    Request.Response.Response = B_Request_Response.ResponseEnum.Success;

                    Connection.Close();
                }
                catch (Exception exception)
                {
                    Base.Error.Handle(exception);

                    Request.Response.Response = B_Request_Response.ResponseEnum.Failure;
                }

                return Request.Response.Response;
            }


            return B_Request_Response.ResponseEnum.Failure;
        }
    }
}
