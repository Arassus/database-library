﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ARP_Base_DLL;
using ARP_Base_DLL.Handler;
using ARP_Base_DLL.Request;

namespace ARP_ADP_DLL
{
    public interface AAD_IDatabaseConnection
    {
        B_Request_Response.ResponseEnum OpenConnection();

        B_Request_Response.ResponseEnum CloseConnection();

        B_Request_Response.ResponseEnum Create_Database();

        B_Request_Response.ResponseEnum Create_Table();

        B_Request_Response.ResponseEnum AssignParameters();

        B_Request_Response.ResponseEnum Select_Table();

        B_Request_Response.ResponseEnum ExecuteQuery(string Query);
    }
}
