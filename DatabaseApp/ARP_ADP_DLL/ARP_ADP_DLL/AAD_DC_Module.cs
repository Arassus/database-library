﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARP_ADP_DLL
{
    public class AAD_DC_Module
    {
    }

    public interface AAD_DC_IModule
    {
        AAD_DatabaseConnection.Response SetupConnectionParameters();

        AAD_DatabaseConnection.Response CheckConnection();
    }
}
