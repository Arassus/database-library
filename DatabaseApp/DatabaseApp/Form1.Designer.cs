﻿namespace DatabaseApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(539, 527);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1_Data;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1_Create;
        private System.Windows.Forms.GroupBox groupBox1_Create_Server;
        private System.Windows.Forms.Button button4_Create_CheckConnection;
        private System.Windows.Forms.GroupBox groupBox4_Create_Database;
        private System.Windows.Forms.TabControl tabControl1_Data;
        private System.Windows.Forms.TabPage tabPage1_Create;
        private System.Windows.Forms.TabPage tabPage2_Connect;
        private System.Windows.Forms.GroupBox groupBox2_Query;
        private System.Windows.Forms.TabControl tabControl2_Query;
        private System.Windows.Forms.TabPage tabPage3_Query_Create;
        private System.Windows.Forms.GroupBox groupBox3_;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox5_Create_Table;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox textBox5_Create_Database_Name;
        private System.Windows.Forms.Button button5_Create_CreateDatabase;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4_Data;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button button6_Data_UpdateDatabase;
        private System.Windows.Forms.Button button7_Data_SynchronizeWithDatabase;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox_Connect_Server;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Connect_Server;
        private System.Windows.Forms.Button button_Connect_Server_Connect;
        private System.Windows.Forms.CheckBox checkBox_Connect_Server_UserInstance;
        private System.Windows.Forms.CheckBox checkBox_Connect_Server_IntegratedSecurity;
        private System.Windows.Forms.TextBox textBox_Connect_Server_Address;
        private System.Windows.Forms.CheckBox checkBox_Connect_Server_Local;
        private System.Windows.Forms.GroupBox groupBox_Connect_Database;
        private System.Windows.Forms.ComboBox comboBox_Connect_Database;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox_Data_DisplayedQuery;
        private System.Windows.Forms.TextBox textBox_Data_DisplayedQuery;
        private System.Windows.Forms.ComboBox comboBox_Connect_Tables;
        private System.Windows.Forms.Button button_Create_Table_Columns_Add;
        private System.Windows.Forms.GroupBox groupBox_Create_Database_Name;
        private System.Windows.Forms.GroupBox groupBox_Create_Database_Server;
        private System.Windows.Forms.TextBox textBox_Create_Database_Server;
        private System.Windows.Forms.Button button_Create_Database_Connect;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox checkBox_Create_Database_Server_Local;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox_Create_Database_Server_IntegratedSecurity;
        private System.Windows.Forms.CheckBox checkBox_Create_Database_Server_User_Instance;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.GroupBox groupBox_Create_Table_Server;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.CheckBox checkBox_Create_Table_Server_UserInstance;
        private System.Windows.Forms.CheckBox checkBox_Create_Table_Server_IntegratedSecurity;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_Create_Table_Server_Connect;
        private System.Windows.Forms.TextBox textBox_Create_Table_Server;
        private System.Windows.Forms.CheckBox checkBox_Create_Table_Server_Local;
        private System.Windows.Forms.GroupBox groupBox_Create_Table_Database;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.ComboBox comboBox_Create_Table_Database;
        private System.Windows.Forms.Button button_Create_Table_Create;
        private System.Windows.Forms.GroupBox groupBox_Create_Table_Table;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.GroupBox groupBox_Server_Server;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.CheckBox checkBox3_Create_UserInstance;
        private System.Windows.Forms.TextBox textBox4_Create_Server;
        private System.Windows.Forms.CheckBox checkBox2_Create_IntegratedSecurity;
        private System.Windows.Forms.CheckBox checkBox1_Create_Local;
        private System.Windows.Forms.GroupBox groupBox_Create_Table_Columns_Edit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label_Create_Table_Column_Name;
        private System.Windows.Forms.TextBox textBox_Create_Table_Column_Name;
        private System.Windows.Forms.Label label_Create_Table_Column_Type;
        private System.Windows.Forms.ComboBox comboBox_Create_Table_Column_Type;
        private System.Windows.Forms.Label label_Create_Table_Column_Size;
        private System.Windows.Forms.Label label_Create_Table_Column_Nullable;
        private System.Windows.Forms.Label label_Create_Table_Column_PrimaryKey;
        private System.Windows.Forms.Label label_Create_Table_Column_ForeignKey;
        private System.Windows.Forms.Button button_Create_Table_Columns_Edit_Apply;
        private System.Windows.Forms.Button button_Create_Table_Columns_Edit_Cancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.ListView listView_Create_Table_Columns;
        private System.Windows.Forms.ColumnHeader columnHeader_Name;
        private System.Windows.Forms.ColumnHeader columnHeader_Type;
        private System.Windows.Forms.ColumnHeader columnHeader_Size;
        private System.Windows.Forms.ColumnHeader columnHeader_Nullable;
        private System.Windows.Forms.ColumnHeader columnHeader_PrimaryKey;
        private System.Windows.Forms.ColumnHeader columnHeader_ForeignKey;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Button button_Create_Table_Columns_Edit;
        private System.Windows.Forms.Button button_Create_Table_Columns_Remove;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox_Create_Table_Name;
        private System.Windows.Forms.TextBox textBox_Create_Table_Column_Size;
        private System.Windows.Forms.ComboBox comboBox_Create_Table_Column_Nullable;
        private System.Windows.Forms.ComboBox comboBox_Create_Table_Column_PrimaryKey;
        private System.Windows.Forms.ComboBox comboBox_Create_Table_Column_ForeignKey;
        private System.Windows.Forms.GroupBox groupBox_Create_Table_Columns;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label_Create_Table_Column_Unique;
        private System.Windows.Forms.Label label_Create_Table_Column_DefaultValue;
        private System.Windows.Forms.ComboBox comboBox_Create_Table_Column_Unique;
        private System.Windows.Forms.TextBox textBox_Create_Table_Column_DefaultValue;
        private System.Windows.Forms.ColumnHeader columnHeader_Unique;
        private System.Windows.Forms.ColumnHeader columnHeader_DefaultValue;
        private System.Windows.Forms.Button button1;
    }
}

