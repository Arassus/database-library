﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ARP_ADP_DLL;
using ARP_Base_DLL;

using ARP_Base_DLL.Request;
using ARP_ADP_DLL.SQL_MY;

namespace DatabaseApp
{
    public partial class Form1 : Form
    {
        //public DataHandler dataHandler;

        //public AAD_DatabaseConnection connection = new AAD_DatabaseConnection();
        

        public Form1()
        {
            InitializeComponent();

            Base.Initialize(MsgBox, Warning, ErrorHandler);
        }

        string LocalServer = @"(localdb)\MSSQLLocalDB";
        string SelectDatabases = "select database_id,name from sys.databases;";


        public static void ErrorHandler(params object[] parameters)
        {
            MessageBox.Show(
                ((Exception) parameters[0]).Message,
                "Error has occurred");
        }

        public static void MsgBox(params object[] parameters)
        {
            if(parameters.Length > 1)
            {
                if ((B_Handler.PriorityEnum)parameters[1] == B_Handler.PriorityEnum.High)
                    MessageBox.Show(
                        parameters.Length > 0 ? parameters[0].ToString() : "",
                        "Message");
            }
        }

        public static void Warning(params object[] parameters)
        {
            MessageBox.Show(
                parameters.Length > 0 ? parameters[0].ToString() : "",
                "Warning");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AAD_DC_MySql Connection = new AAD_DC_MySql();

            Connection.Request.AddParameter("Server", "localhost");
            Connection.Request.AddParameter("port", "3306");
            Connection.Request.AddParameter("database", "simpletest");
            Connection.Request.AddParameter("user", "testuser");
            Connection.Request.AddParameter("password", "Kret84Mysql");


            //DataSet data = new DataSet();
            //DataTable TestingTable = new DataTable("TestingTableBeta");
            //data.Tables.Add(TestingTable);



            //Connection.Request.AddParameter("dataset", data);

            //Connection.Select_Table();

            //Connection.Local_Data.Tables[0].Rows[2].SetField(0, "EDITED 1");

            //Connection.Local_Data.Tables[0].Rows[5].SetField(0, "EDITED 2");

            //var Response = Connection.Update();

            //MessageBox.Show(Response.ToString());


            //var Response = Connection.List_Databases();
            //var Response = Connection.List_Tables();

            DataSet data = new DataSet();
            DataTable TestingTable = new DataTable("TestingTableBeta");
            data.Tables.Add(TestingTable);
            Connection.Request.AddParameter("dataset", data);

            var Response = Connection.Describe_Table();

            MessageBox.Show(Response.ToString());

            string RowString = "";

            foreach (DataColumn Column in Connection.Local_Data.Tables[0].Columns)
            {
                RowString += Column.ColumnName + " |";
            }

            MessageBox.Show(RowString, "Columns");

            foreach (DataRow Row in Connection.Local_Data.Tables[0].Rows)
            {
                RowString = "";

                foreach(object Value in Row.ItemArray)
                {
                    RowString += Value.ToString() + " |";
                }

                MessageBox.Show(RowString,"Value");
            }
        }
        
    }


}
