﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataLib
{
    public class DataHandler
    {
        public delegate void Handler(params object[] parameters);

        Handler ErrorHandler;

        Handler MessageHandler;

        public void Initialize(Handler ErrorBehaviour, Handler MessageBehaviour)
        {
            ErrorHandler = ErrorBehaviour;

            MessageHandler = MessageBehaviour;
        }

        //string 
        //    Server = "",
        //    Database = "",
        //    User = "",
        //    Password = "";

        //bool 
        //    IntegratedSecurity = false,
        //    UserInstance = false;

        public string CreateMdfFile(string Database)
        {
            try
            {
                SaveFileDialog dialog = new SaveFileDialog();

                dialog.Filter = "SQL Server Database File (*.mdf)|*.mdf|All files (*.*)|*.*";

                dialog.FilterIndex = 1;

                dialog.RestoreDirectory = true;

                dialog.InitialDirectory = Environment.CurrentDirectory;

                dialog.FileName = Database;

                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.OK) 
                {
                    MessageHandler(dialog.FileName);

                    return dialog.FileName;
                }
            }
            catch (Exception exception)
            {
                ErrorHandler(exception);
            }

            return "";
        }

        public void CreateDatabase(string Server, string Database, string IntegratedSecurity, string UserInstance)
        {
            try
            {
                using (var connection = new SqlConnection(String.Format(
                    "Data Source=" + Server + "; " +
                    "Initial Catalog=" + Database + "; " +
                    "Integrated Security=" + IntegratedSecurity + "; " +
                    "User Instance=" + UserInstance + "; "
                    )))
                {
                    connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = String.Format("if not exists(select * from sys.databases where name = '"+ Database + "') create database {databaseName}");

                        int response = command.ExecuteNonQuery();

                        if(response == 0)
                        {
                            MessageHandler("Database '{databaseName}' already exists on server '{Server}'");
                        }
                        else
                        {
                            MessageHandler("Database '{databaseName}' has been created successfully on server '{Server}'");
                        }
                    }

                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                ErrorHandler(exception);
            }
        }

        public void CheckConnectionToServer(string Server, string IntegratedSecurity, string UserInstance)
        {
            string ConnectionString = String.Format(
                //@"Data Source=(LocalDB)\MSSQLLocalDB; Integrated Security=true;"
                "Data Source=" + Server + ";" +
                "Integrated Security=" + IntegratedSecurity + ";" +
                "User Instance=" + UserInstance + ";"
                );

            MessageHandler(ConnectionString);

            try
            {

                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    MessageHandler("Successfully connected to server" + Environment.NewLine + "'" + Server + "'");

                    connection.Close();
                }
            }
            catch(Exception exception)
            {
                ErrorHandler(exception);
            }
        }

        public DataTable GetAllDatabases(string Server, string IntegratedSecurity, string UserInstance)
        {
            DataTable table = new DataTable();

            string ConnectionString = String.Format(
                "Data Source=" + Server + ";" +
                "Integrated Security=" + IntegratedSecurity + ";" +
                "User Instance=" + UserInstance + ";"
                );

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT database_id,name FROM master.sys.databases;", connection); //c.con is the connection string

                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                    table.Locale = System.Globalization.CultureInfo.InvariantCulture;

                    dataAdapter.Fill(table);

                    MessageHandler("Successfully connected to server" + Environment.NewLine + "'" + Server + "'");

                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                ErrorHandler(exception);
            }

            return table;
        }
    }
}
